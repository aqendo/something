
#include <iostream>
#include <filesystem>
#include <vector>
using namespace std;

int main() {
    int n, k;
    cin >> n >> k;
    vector<int> hist(n);
    for (int i = 0; i < n; ++i) {
        cin >> hist[i];
    }
    int mx=-1,my=-1,mxindex=-1,myindex=-1;
    for (int i = 1; i < n; ++i) {
        if (hist[i] + hist[i - 1] > mx+my) {
            mx = hist[i-1];
            my = hist[i];
            mxindex = i - 1;
            myindex = i;
        }
    }
    int maxfromall = mx + my;
    int index_old_x = mxindex;
    int index_old_y = myindex;
    mx=-1, my=-1, mxindex=-1, myindex=-1;
    for (int i = 1; i < n; ++i) {
        if (i == index_old_x) {
            i+=2;
            continue;
        } else if (i == index_old_y) {
            i++;
            continue;
        }
        if (hist[i] + hist[i - 1] > mx + my) {
            mx = hist[i-1];
            my = hist[i];
            mxindex = i - 1;
            myindex = i;
        }

    }
    cout << mx + my << "\n";
    return 0;
}
